$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

  });

    /* mostrar modal */
    $('#nosotros').on('show.bs.modal', function(e){
        
        $("#Btnnosotros").prop('disabled', true);
        console.log('ventana abierta');
        

        $('#Btnnosotros').removeClass('nav-link');
        $('#Btnnosotros').addClass('Btndesabilitado');
        console.log('boton cambio de color');


        console.log('boton desactivado');
    });

    $('#nosotros').on('shown.bs.modal', function(e){
        console.log('ventana abriendo');
    });

    /* fin mostrar modal */


    /* cerrar modal */


    $('#nosotros').on('hide.bs.modal', function(e){
        
        $('#Btnnosotros').removeClass('Btndesabilitado');
        $('#Btnnosotros').addClass('nav-link');
        console.log('abilitando boton');
        console.log('ventana cerrada');


        $("#Btnnosotros").prop('disabled', false);
        console.log('boton activado');
    });

    $('#nosotros').on('hidden.bs.modal', function(e){
        console.log('ventana cerrando');
    });

    /* Fin cerrar Modal */